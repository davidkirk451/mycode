#!/usr/bin/env python3
'''Alta3 Research | RZFeeser
   Using Python to parse JSON - Operations Engineers are very often tasked with work with JSON data, as it is a popular response type to API requests.'''

# standard library import
import json # working with JSON data

def main():
    '''open a file containing JSON with python'''
    
    # open the JSON file in read-mode with the handle jf
    with open("poweredge.json", "r") as jf:
        # transfrom JSON to python data structures
        fog = json.load(jf)
    
    # when we stop indenting, jf is auto-closed
    
    # display the length of fog['results']['msg']
    print("The value returned with 'msg' was: ", fog['results']['msg'])

    # Return the controllers
    print("The number of disk enclosures:", fog.get('results').get('storage_status').get('Message').get('Controller').keys())

    
# best practice technique to call our python script
if __name__ == "__main__":
    main()     # calls the "main" function to run

