# mycode (Project Title)

Ansible 202 - continue learning how to version control projects with gi.


## Getting Started

These instructions will get you a copy of the project up and running on your local machine
for development and testing purposes. See deployment for notes on how to deploy the project
on a live system.

### Prerequisites

What things are needed to install the software and how to install them. For now, maybe copy in:
"How to Install the Language: "

## Built With

* [Ansible](https://www.ansible.com) - The coding language used
* [Python](https://www.python.org/) - The coding language used

## Authors

David Kirk - *Initial work*
